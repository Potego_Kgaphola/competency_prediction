<!-- Bootstrap Js -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<!-- Synaptic Js -->
<script type="text/javascript" src="js/synaptic.min.js"></script>
<script type="text/javascript" src="js/underscore-min.js"></script>
<!-- Chart Js -->
<script type="text/javascript" src="js/chart.min.js"></script>  		
<!-- Morris Chart Js -->
<script src="js/easypiechart.js"></script>
<script src="js/easypiechart-data.js"></script>
