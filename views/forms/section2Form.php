<div class="row justify-content-md-center">
    <div class="col-md-9">
		<form action ="" name="form1" method = "get">
			<div class="card">
            <article class="card-body">
				<h4 class="card-title mb-4 mt-1">This section is going to assess how you act when in different situations.</h4>			
			<div class="form-group">
			<table class = "table">
			<th>Question</th>
			<th>Strongly Agree</th>
			<th>Agree</th>
			<th>Sometimes</th>
			<th>Disagree</th>
			<th>Strongly Disagree</th>
					<?php					
						foreach ($section2_questions as $key => $section2) {
							echo"<tr>
							<td>".$section2['question_desc']."</td>";
							
							for($x = 5; $x > 0; $x--){
								echo
								'<td>	
									<div class="form-check">
									  <input name="section2_score'.$key.'" class="form-check-input" type="radio" value="'.$x.'" required>
									</div>
								</td>';
								
							}
							echo '</tr>';
						}
					?>
			</table>
				<input type ="text" value="<?= $s1?>" name="score1" hidden>
				<button type="submit" name = "btnSection2" class="btn btn-outline-primary">Continue</button>
				</div>
				</article>
            </div> 
         </form>
    </div>