<div class="container">
	<div class="row justify-content-md-center" style="margin-top:15px;">
		<div class="card col-md-9">
			<h4 class="card-title mb-4 mt-1">Sign In</h4>
			<hr>		
			<form action="" method="POST"> 
				<div class="form-group row">
					<label class="col-sm-4 col-form-label">Student No.</label>			
					<input type="text" class="form-control col-sm-4" name="studno"></input>
				</div>				
				<div class="form-group row">
					<label class="col-sm-4 col-form-label">Password</label>			
					<input type="password" class="form-control col-sm-4" name="passwd"></input>
				</div>
				<div class="form-group row">
					<label class="col-sm-4 col-form-label">Select qualification to enroll for</label>
					<select name="qualification" class="form-check col-sm-4" id="qualification">
					<?php foreach($qualifications as $key => $qual){
							echo "<option value ='".$qual['q_id']."'>".$qual['q_name']." (".$qual['q_code'].")</option>";
						}
					?>
					</select>
				</div>				
				<div class="form-group row">
					<div class="col-md-2">
						<button type="submit" name="submit-login" class="btn btn-primary">Submit</button>
					</div>
				</div>                                                          		   
			</form>
		 </div>
	</div>
 </div>
