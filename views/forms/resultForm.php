<div class="container-fluid" style="margin-top:3.8%">
<div class="row">
<div class="col-sm-6 col-xs-12">

		<div class="panel panel-default chartJs">
			<div class="panel-heading">
				<div class="card-title">
					<div class="title">Competency Prediction</div>
				</div>
			</div>
			<div class="panel-body">
					<div class="panel-body easypiechart-panel">

						<div class="easypiechart" id="easypiechart-black" data-percent="<?=$prediction; ?>" ><span class="percent"><?= $prediction; ?>%</span>
												
						</div>
						<h4 id ="status">Competency for <?= $qualification; ?></h4>
						<h4><?= $recommendation; ?></h4>
					</div>
			</div>
		</div>

</div>
<div class="col-sm-6 col-xs-12">

		<div class="panel panel-default chartJs">
			<div class="panel-heading">
				<div class="card-title">
					<div class="title">Brain Qaudrant Dominance</div>
				</div>
			</div>
			<div class="panel-body">
				<canvas id="polar-area-chart" class="chart"></canvas>
			</div>
		</div>
</div>
</div>	

		<div class="row">
			<div class="col-xs-6 col-md-3">
				<div class="panel panel-default">
					<div class="panel-body easypiechart-panel">
						<h4>Upper Left Brain Usage (L1)</h4>
						<div class="easypiechart" id="easypiechart-blue" data-percent="<?= $quadrant_1; ?>" ><span class="percent"><?= $quadrant_1; ?>%</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-6 col-md-3">
				<div class="panel panel-default">
					<div class="panel-body easypiechart-panel">
						<h4>Upper Right Brain Usage (R1)</h4>
						<div class="easypiechart" id="easypiechart-orange" data-percent="<?= $quadrant_3; ?>" ><span class="percent"><?= $quadrant_3; ?>%</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-6 col-md-3">
				<div class="panel panel-default">
					<div class="panel-body easypiechart-panel">
						<h4>Lower Left Brain Usage (L2)</h4>
						<div class="easypiechart" id="easypiechart-teal" data-percent="<?= $quadrant_2; ?>" ><span class="percent"><?= $quadrant_2; ?>%</span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xs-6 col-md-3">
				<div class="panel panel-default">
					<div class="panel-body easypiechart-panel">
						<h4>Lower Right Brain Usage (R2)</h4>
						<div class="easypiechart" id="easypiechart-red" data-percent="<?= $quadrant_4; ?>" ><span class="percent"><?= $quadrant_4; ?>%</span>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>