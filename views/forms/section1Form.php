<div class="row justify-content-md-center">
    <div class="col-md-9">
		<form action ="" name="form1" method = "get">
			<div class="card" >
            <article class="card-body">

            <h4 class="card-title mb-4 mt-1">This section is going to assess how you apply logic in a situation.</h4>
			
			<div class="form-group">
			<table class = "table">
			<th>Question</th>
			<th>Strongly Agree</th>
			<th>Agree</th>
			<th>Sometimes</th>
			<th>Disagree</th>
			<th>Strongly Disagree</th>
					<?php					
						foreach ($section1_questions as $key => $section1) {
							echo"<tr>
							<td>".$section1['question_desc']."</td>";
							
							for($x = 5; $x > 0; $x--){
								echo
								'<td>	
									<div class="form-check">
									  <input name="section1_score'.$key.'" class="form-check-input" type="radio" value="'.$x.'" required>
									</div>
								</td>';
								
							}
							echo '</tr>';
						}
					?>
			</table>
				<button type="submit" name = "btnSection1" class="btn btn-outline-primary">Continue</button>
				</div>
				</article>
            </div> 
         </form>
    </div>