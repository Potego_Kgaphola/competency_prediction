<?php 
session_start();
if($_SESSION['login'] == false || !isset($_SESSION['login'])){
  header('Location: login.php');
}
require 'controllers/competencyController.php';

$title = "Competency Prediction";

$run = new competencyController();
$section3_questions = $run->getSection3();
	
//$id = $_SESSION['user_id'];

//$userArray = $run->getUser($id);		

$s1 = $_GET['score1'];
$s2 = $_GET['score2'];
$section1_total = 0;
if(isset($_GET['btnSection3'])){
	
	for( $x = 0; $x < count($_GET) - 1; $x++){
		
		$section3_total += $_GET['section3_score'.$x];
	}

	header('Location: section4.php?score1='.$_GET['score1'].'&score2='.$_GET['score2'].'&score3='.$section3_total);
}

include 'views/view.section3.php';