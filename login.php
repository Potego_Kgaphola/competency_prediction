<?php
session_start();
require 'controllers/userController.php';
$title = "Competency Prediction";
$run = new UserController();
$qualifications = $run->getQualification();

if(isset($_POST['submit-login'])){
	$username = !empty($_POST['studno']) ? $_POST['studno'] : null;
	$password = !empty($_POST['passwd']) ? $_POST['passwd'] : null;
	$course = !empty($_POST['qualification']) ? $_POST['qualification'] : null;	
	$run->login($username,$password,$course);
}

require 'views/login.view.php';
?>