<?php 
session_start();
if($_SESSION['login'] == false || !isset($_SESSION['login'])){
  header('Location: login.php');
}
require 'controllers/competencyController.php';

$title = "Competency Prediction";

$run = new competencyController();
$section4_questions = $run->getSection4();
	
//$id = $_SESSION['user_id'];

//$userArray = $run->getUser($id);	
$s1 = $_GET['score1'];
$s2 = $_GET['score2'];
$s3 = $_GET['score3'];
$section4_total = 0;
if(isset($_GET['btnSection4'])){
	
	for( $x = 0; $x < count($_GET) - 1; $x++){
		
		$section4_total += $_GET['section4_score'.$x];
	}
	header('Location: result.php?score1='.$_GET['score1'].'&score2='.$_GET['score2'].'&score3='.$_GET['score3'].'&score4='.$section4_total);
}

include 'views/view.section4.php';