<?php
session_start();
if($_SESSION['login'] == false || !isset($_SESSION['login'])){
  header('Location: login.php');
}
include 'controllers/competencyController.php';
$title = "Competency Prediction";
$run = new competencyController();

$student = $run->getStudent($_SESSION['stud_no']);
$section1_tot = $_GET['score1'];
$section2_tot = $_GET['score2'];
$section3_tot = $_GET['score3'];
$section4_tot = $_GET['score4'];
$stud_aps = $student[0]['stud_aps'];
$qualification_id =  $_SESSION['course'];
$course = $run->getCourse($qualification_id);
$qualification = $course[0]['q_name'];

$scores = array();
$quadrant_array = array('L1','L2','R1','R2');
array_push($scores,$section1_tot,$section2_tot,$section3_tot,$section4_tot);
//$rec_course = array();
$high = $scores[0];
$index = 0;
for($x = 1; $x < sizeof($scores); $x++){
	if($scores[$x] > $high){
		$high = $scores[$x];
		$index = $x;
	}
}
$quad = $quadrant_array[$index];

	if($section1_tot == $section2_tot && $section1_tot == $section3_tot && $section1_tot == $section4_tot){
		$recommendation = "You are a whole brain thinker! whole brain thinkers are capable of excelling in any course.";
	}else if( $index + 1 != $qualification_id){
		$rec_course = $run->getCourse($index + 1);
		$rec_qual = $rec_course[0]['q_name'];
		$recommendation = " You would stand a better chance in ".$rec_qual;
	}else{
		$recommendation = " ";
	}

$train_data = $run->trainData();
$test_data = $run->testData();
$competency = $_COOKIE['prediction'];
$prediction = round($competency*100);



$quadrant_1 = round(($section1_tot/30)*100);
$quadrant_2 = round(($section2_tot/30)*100);
$quadrant_3 = round(($section3_tot/30)*100);
$quadrant_4 = round(($section4_tot/30)*100);

include 'views/view.result.php';
?>