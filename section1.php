<?php 
session_start();
if($_SESSION['login'] == false || !isset($_SESSION['login'])){
  header('Location: login.php');
}
require 'controllers/competencyController.php';
$title = "Competency Prediction";
$run = new competencyController();
$section1_questions = $run->getSection1();	
//$id = $_SESSION['user_id'];
//$userArray = $run->getUser($id);	
$section1_total = 0;
if(isset($_GET['btnSection1'])){
	
	for( $x = 0; $x < count($_GET) - 1; $x++){
		
		$section1_total += $_GET['section1_score'.$x];
	}
	header('Location: section2.php?score1='.$section1_total);
}

include 'views/view.section1.php';