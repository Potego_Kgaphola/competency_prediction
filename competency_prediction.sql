-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2018 at 12:08 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `competency_prediction`
--

-- --------------------------------------------------------

--
-- Table structure for table `assesment`
--

CREATE TABLE `assesment` (
  `assesment_no` int(6) NOT NULL,
  `quad1_score` int(3) DEFAULT NULL,
  `quad2_score` int(3) DEFAULT NULL,
  `quad3_score` int(3) DEFAULT NULL,
  `quad4_score` int(3) DEFAULT NULL,
  `dominant_quad` varchar(2) DEFAULT NULL,
  `student_no` varchar(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quadrant`
--

CREATE TABLE `quadrant` (
  `quadrant_code` varchar(2) NOT NULL,
  `quadrant_desc` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quadrant`
--

INSERT INTO `quadrant` (`quadrant_code`, `quadrant_desc`) VALUES
('L1', 'Left Brain Quadrant 1'),
('L2', 'Left Brain Quadrant 2'),
('R1', 'Right Brain Quadrant 1'),
('R2', 'Right Brain Quadrant 2');

-- --------------------------------------------------------

--
-- Table structure for table `qualification`
--

CREATE TABLE `qualification` (
  `q_id` int(4) NOT NULL,
  `q_name` varchar(50) DEFAULT NULL,
  `q_code` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qualification`
--

INSERT INTO `qualification` (`q_id`, `q_name`, `q_code`) VALUES
(1, 'Finance and Accounting', 'NDFA99'),
(2, 'Information Technology', 'NDIT12'),
(3, 'Graphic Design', 'NDGD04'),
(4, 'Marketing', 'NDMK95');

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `question_no` int(3) NOT NULL,
  `question_desc` varchar(255) DEFAULT NULL,
  `quad_code` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`question_no`, `question_desc`, `quad_code`) VALUES
(1, 'I work in my own self-inspired direction.', 'L1'),
(2, 'I like using the \"tried and true\" method.', 'L1'),
(3, 'I leave my work out on my desk so I can work as I am inspired by ideas.', 'R1'),
(4, 'I like creating new methods for doing things.', 'R1'),
(5, 'I am a very flexible and unpredictable person.', 'R1'),
(6, 'I spread my work evenly over the time I have.', 'L2'),
(7, 'I keep everything in a particular place.', 'L2'),
(8, 'I complete one project at a time.', 'L2'),
(9, 'I follow directions carefully when I build a model, make a craft,etc.', 'L2'),
(10, 'At home, my room has organized drawer and closets. I even try to organize other thingsb around the house.', 'L2'),
(11, 'With a hard decision, I choose what I feel is right.', 'R2'),
(12, 'I usually act on my feelings.', 'R2'),
(13, 'My thinking is like words going through my head.', 'R2'),
(14, 'I can sense what is going to happen next.', 'R2'),
(15, 'I like to direct the work of others.', 'L2'),
(16, 'When I talk I often use hand guestures.', 'R2'),
(17, 'I like helping others or teaching others.', 'R2'),
(18, 'I prefare learning from pictures and videos.', 'R1'),
(19, 'I like drawing and paiting.', 'R1'),
(20, 'I like surprises and taking risks.', 'R1'),
(21, 'I do well in word games(scrabble).', 'L1'),
(22, 'I enjoy reading and writing.', 'L1'),
(23, 'I like working with numbers.', 'L1'),
(24, 'I\'m good at solving problems.', 'L1');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `stud_no` varchar(9) NOT NULL,
  `stud_name` varchar(30) DEFAULT NULL,
  `stud_surname` varchar(30) DEFAULT NULL,
  `stud_email` varchar(40) DEFAULT NULL,
  `stud_phone` varchar(10) DEFAULT NULL,
  `stud_aps` int(2) DEFAULT NULL,
  `password` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`stud_no`, `stud_name`, `stud_surname`, `stud_email`, `stud_phone`, `stud_aps`, `password`) VALUES
('211345876', 'John', 'Doe', 'johndoe@email.com', '0733569873', 27, 'password'),
('212458097', 'Luke', 'Makua', 'lukemakua@email.com', '0639204785', 22, 'password'),
('213498567', 'Jane', 'Coles', 'janecoles@email.com', '0785631095', 18, 'password'),
('213654785', 'Mary', 'Jones', 'maryjones@email.com', '0845671234', 30, 'password'),
('214387500', 'Thabo', 'Mokwena', 'thabomokwena@email.com', '0745670935', 33, 'password'),
('215009562', 'Patric', 'Maluleke', 'patricmaluleke@email.com', '0768903278', 19, 'password');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assesment`
--
ALTER TABLE `assesment`
  ADD PRIMARY KEY (`assesment_no`),
  ADD KEY `FK_assesment_stud` (`student_no`),
  ADD KEY `FK_assesment_quad` (`dominant_quad`);

--
-- Indexes for table `quadrant`
--
ALTER TABLE `quadrant`
  ADD PRIMARY KEY (`quadrant_code`);

--
-- Indexes for table `qualification`
--
ALTER TABLE `qualification`
  ADD PRIMARY KEY (`q_id`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`question_no`),
  ADD KEY `FK_question_quad` (`quad_code`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`stud_no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assesment`
--
ALTER TABLE `assesment`
  MODIFY `assesment_no` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `qualification`
--
ALTER TABLE `qualification`
  MODIFY `q_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `question_no` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assesment`
--
ALTER TABLE `assesment`
  ADD CONSTRAINT `FK_assesment_quad` FOREIGN KEY (`dominant_quad`) REFERENCES `quadrant` (`quadrant_code`),
  ADD CONSTRAINT `FK_assesment_stud` FOREIGN KEY (`student_no`) REFERENCES `student` (`stud_no`);

--
-- Constraints for table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `FK_question_quad` FOREIGN KEY (`quad_code`) REFERENCES `quadrant` (`quadrant_code`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
