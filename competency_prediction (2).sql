-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 04, 2019 at 11:28 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `competency_prediction`
--

-- --------------------------------------------------------

--
-- Table structure for table `assesment`
--

CREATE TABLE `assesment` (
  `assesment_no` int(6) NOT NULL,
  `quad1_score` int(3) DEFAULT NULL,
  `quad2_score` int(3) DEFAULT NULL,
  `quad3_score` int(3) DEFAULT NULL,
  `quad4_score` int(3) DEFAULT NULL,
  `dominant_quad` varchar(2) DEFAULT NULL,
  `student_no` varchar(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quadrant`
--

CREATE TABLE `quadrant` (
  `quadrant_code` varchar(2) NOT NULL,
  `quadrant_desc` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quadrant`
--

INSERT INTO `quadrant` (`quadrant_code`, `quadrant_desc`) VALUES
('L1', 'Left Brain Quadrant 1'),
('L2', 'Left Brain Quadrant 2'),
('R1', 'Right Brain Quadrant 1'),
('R2', 'Right Brain Quadrant 2');

-- --------------------------------------------------------

--
-- Table structure for table `qualification`
--

CREATE TABLE `qualification` (
  `q_id` int(4) NOT NULL,
  `q_name` varchar(50) DEFAULT NULL,
  `q_code` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qualification`
--

INSERT INTO `qualification` (`q_id`, `q_name`, `q_code`) VALUES
(1, 'Finance and Accounting', 'NDFA99'),
(2, 'Information Technology', 'NDIT12'),
(3, 'Graphic Design', 'NDGD04'),
(4, 'Marketing', 'NDMK95');

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `question_no` int(3) NOT NULL,
  `question_desc` varchar(255) DEFAULT NULL,
  `quad_code` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`question_no`, `question_desc`, `quad_code`) VALUES
(1, 'I work in my own self-inspired direction.', 'L1'),
(2, 'I like using the \"tried and true\" method.', 'L1'),
(3, 'I leave my work out on my desk so I can work as I am inspired by ideas.', 'R1'),
(4, 'I like creating new methods for doing things.', 'R1'),
(5, 'I am a very flexible and unpredictable person.', 'R1'),
(6, 'I spread my work evenly over the time I have.', 'L2'),
(7, 'I keep everything in a particular place.', 'L2'),
(8, 'I complete one project at a time.', 'L2'),
(9, 'I follow directions carefully when I build a model, make a craft,etc.', 'L2'),
(10, 'At home, my room has organized drawer and closets. I even try to organize other things around the house.', 'L2'),
(11, 'With a hard decision, I choose what I feel is right.', 'R2'),
(12, 'I usually act on my feelings.', 'R2'),
(13, 'My thinking is like words going through my head.', 'R2'),
(14, 'I can sense what is going to happen next.', 'R2'),
(15, 'I like to direct the work of others.', 'L2'),
(16, 'When I talk I often use hand guestures.', 'R2'),
(17, 'I like helping others or teaching others.', 'R2'),
(18, 'I prefare learning from pictures and videos.', 'R1'),
(19, 'I like drawing and paiting.', 'R1'),
(20, 'I like surprises and taking risks.', 'R1'),
(21, 'I do well in word games(scrabble).', 'L1'),
(22, 'I enjoy reading and writing.', 'L1'),
(23, 'I like working with numbers.', 'L1'),
(24, 'I\'m good at solving problems.', 'L1');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `stud_no` varchar(9) NOT NULL,
  `stud_name` varchar(30) DEFAULT NULL,
  `stud_surname` varchar(30) DEFAULT NULL,
  `stud_email` varchar(40) DEFAULT NULL,
  `stud_phone` varchar(10) DEFAULT NULL,
  `stud_aps` int(2) DEFAULT NULL,
  `password` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`stud_no`, `stud_name`, `stud_surname`, `stud_email`, `stud_phone`, `stud_aps`, `password`) VALUES
('211345876', 'John', 'Doe', 'johndoe@email.com', '0733569873', 27, 'password'),
('212458097', 'Luke', 'Makua', 'lukemakua@email.com', '0639204785', 22, 'password'),
('213498567', 'Jane', 'Coles', 'janecoles@email.com', '0785631095', 18, 'password'),
('213654785', 'Mary', 'Jones', 'maryjones@email.com', '0845671234', 30, 'password'),
('214387500', 'Thabo', 'Mokwena', 'thabomokwena@email.com', '0745670935', 33, 'password'),
('215009562', 'Patric', 'Maluleke', 'patricmaluleke@email.com', '0768903278', 19, 'password');

-- --------------------------------------------------------

--
-- Table structure for table `test_data`
--

CREATE TABLE `test_data` (
  `L1_score` int(4) DEFAULT NULL,
  `L2_score` int(4) DEFAULT NULL,
  `R1_score` int(4) DEFAULT NULL,
  `R2_score` int(4) DEFAULT NULL,
  `dominant` varchar(2) DEFAULT NULL,
  `Aps` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_data`
--

INSERT INTO `test_data` (`L1_score`, `L2_score`, `R1_score`, `R2_score`, `dominant`, `Aps`) VALUES
(27, 19, 9, 18, 'L2', 34),
(30, 24, 19, 9, 'R1', 31),
(18, 8, 17, 12, 'L2', 38),
(29, 26, 17, 21, 'L1', 42),
(20, 11, 29, 21, 'R1', 28),
(18, 13, 22, 20, 'R2', 31),
(12, 11, 20, 22, 'L1', 31),
(16, 15, 28, 24, 'R1', 42),
(12, 19, 21, 15, 'L2', 39),
(18, 22, 9, 28, 'L2', 27),
(23, 17, 15, 19, 'R2', 29),
(9, 24, 6, 24, 'R1', 35),
(19, 11, 12, 17, 'R1', 22),
(24, 27, 11, 24, 'L1', 19),
(13, 6, 24, 21, 'R1', 22),
(9, 17, 23, 9, 'R1', 27),
(16, 24, 20, 26, 'L2', 42),
(26, 28, 13, 22, 'R2', 27),
(8, 16, 19, 12, 'R2', 24),
(11, 12, 26, 13, 'R2', 23),
(14, 30, 27, 13, 'L2', 19),
(28, 14, 25, 29, 'R1', 28),
(6, 28, 18, 6, 'R1', 21),
(27, 7, 10, 10, 'L1', 37),
(26, 26, 6, 14, 'L1', 37),
(6, 23, 7, 16, 'L1', 26),
(25, 26, 12, 19, 'L2', 33),
(6, 27, 12, 11, 'R2', 34),
(27, 18, 19, 20, 'R2', 31),
(24, 25, 20, 18, 'R1', 42),
(12, 15, 10, 21, 'R1', 23),
(25, 26, 29, 28, 'R1', 34),
(19, 18, 25, 22, 'R2', 21),
(8, 9, 23, 13, 'L1', 30),
(9, 28, 18, 24, 'L1', 28),
(15, 11, 20, 22, 'L1', 23),
(26, 14, 17, 25, 'L2', 35),
(8, 29, 15, 10, 'R2', 41),
(30, 9, 7, 23, 'R2', 35),
(17, 25, 22, 17, 'R2', 20),
(11, 28, 30, 6, 'R2', 33),
(17, 27, 21, 10, 'R1', 33),
(9, 20, 19, 26, 'L1', 33),
(12, 17, 22, 12, 'L2', 26),
(6, 16, 27, 9, 'R2', 29),
(9, 30, 17, 28, 'R2', 38),
(12, 24, 10, 11, 'L1', 37),
(11, 6, 24, 10, 'L2', 42),
(25, 15, 22, 22, 'L1', 31),
(27, 17, 7, 14, 'L1', 26),
(22, 27, 6, 22, 'L2', 40),
(20, 25, 9, 21, 'L1', 29),
(27, 7, 23, 19, 'L2', 29),
(20, 28, 11, 30, 'L1', 34),
(6, 7, 19, 6, 'L2', 41),
(20, 29, 17, 18, 'L2', 30),
(15, 26, 22, 23, 'R1', 22),
(9, 7, 15, 24, 'L2', 20),
(12, 24, 20, 13, 'R2', 27),
(17, 22, 20, 28, 'L2', 29),
(8, 21, 17, 24, 'L2', 27),
(22, 27, 27, 25, 'L1', 23),
(30, 17, 20, 23, 'R1', 29),
(30, 22, 6, 17, 'L2', 34),
(14, 23, 8, 12, 'R1', 18),
(6, 20, 12, 7, 'L1', 28),
(26, 6, 26, 13, 'R2', 42),
(21, 28, 6, 28, 'R1', 37),
(13, 12, 8, 10, 'L1', 30),
(22, 29, 30, 19, 'R2', 18),
(10, 16, 25, 29, 'R2', 32),
(26, 14, 9, 9, 'R1', 25),
(13, 30, 10, 6, 'R2', 33),
(23, 20, 23, 20, 'L2', 19),
(30, 29, 22, 21, 'R2', 21),
(19, 28, 27, 20, 'R2', 35),
(24, 12, 14, 29, 'R2', 35),
(13, 10, 28, 17, 'R2', 34),
(26, 7, 6, 14, 'L2', 30),
(26, 15, 25, 30, 'L1', 23),
(16, 24, 27, 14, 'L2', 42),
(16, 27, 9, 15, 'L1', 27),
(6, 21, 29, 26, 'L1', 18),
(10, 21, 12, 10, 'L2', 33),
(20, 8, 24, 26, 'R2', 28),
(25, 23, 21, 30, 'R2', 35),
(18, 26, 22, 12, 'L1', 22),
(18, 21, 9, 29, 'R1', 35),
(23, 13, 28, 9, 'R1', 27),
(24, 6, 25, 25, 'R2', 38),
(14, 28, 19, 30, 'L2', 40),
(20, 8, 16, 24, 'R2', 25),
(29, 15, 14, 21, 'L2', 18),
(13, 25, 21, 20, 'R2', 37),
(23, 28, 18, 21, 'R1', 36),
(26, 8, 16, 12, 'L1', 25),
(18, 11, 12, 26, 'L2', 33),
(27, 26, 28, 7, 'R2', 22),
(7, 9, 17, 17, 'R1', 29),
(12, 18, 15, 7, 'R2', 27),
(23, 20, 29, 18, 'R2', 38),
(6, 27, 19, 24, 'L1', 31),
(15, 29, 22, 15, 'L2', 33),
(21, 7, 11, 15, 'L1', 19),
(25, 13, 16, 6, 'R1', 24),
(22, 12, 15, 22, 'R2', 31),
(7, 17, 20, 20, 'R2', 23),
(7, 29, 12, 6, 'L2', 22),
(23, 23, 20, 15, 'R2', 30),
(10, 13, 13, 8, 'L2', 33),
(25, 7, 28, 27, 'R2', 23),
(30, 25, 29, 10, 'R1', 33),
(24, 28, 22, 30, 'L1', 26),
(6, 28, 20, 12, 'L1', 38),
(7, 14, 27, 30, 'R2', 39),
(8, 21, 24, 27, 'R1', 21),
(16, 27, 21, 14, 'R2', 29),
(17, 8, 17, 11, 'L2', 36),
(19, 17, 6, 27, 'R1', 22),
(27, 8, 20, 16, 'R1', 35),
(13, 18, 7, 13, 'L2', 39),
(6, 21, 20, 26, 'R2', 20),
(16, 9, 18, 19, 'R1', 30),
(7, 25, 13, 22, 'R1', 24),
(17, 24, 15, 8, 'L2', 19),
(16, 17, 9, 9, 'R1', 20),
(20, 23, 8, 30, 'L2', 40),
(23, 29, 25, 22, 'L1', 25),
(24, 7, 20, 9, 'R1', 31),
(8, 9, 21, 10, 'L1', 41),
(29, 22, 9, 25, 'R1', 40),
(8, 13, 27, 6, 'R2', 21),
(14, 13, 8, 7, 'R1', 40),
(29, 29, 12, 29, 'L2', 38),
(27, 22, 12, 30, 'R1', 34),
(8, 16, 22, 19, 'L1', 18),
(30, 21, 8, 9, 'L2', 22),
(27, 8, 28, 20, 'R2', 18),
(16, 13, 9, 25, 'R2', 22),
(12, 14, 13, 16, 'R2', 40),
(7, 12, 21, 7, 'R2', 38),
(30, 21, 13, 16, 'R1', 42),
(12, 12, 14, 14, 'L2', 38),
(10, 15, 30, 25, 'R2', 30),
(19, 24, 14, 24, 'L1', 31),
(14, 23, 10, 30, 'R2', 40),
(6, 18, 13, 28, 'L1', 38),
(11, 19, 12, 17, 'L2', 32),
(13, 17, 25, 28, 'L1', 32),
(10, 17, 15, 20, 'R1', 24),
(9, 18, 9, 27, 'L2', 18),
(12, 26, 23, 15, 'L2', 42),
(17, 27, 27, 21, 'L1', 32),
(25, 28, 27, 21, 'L2', 27),
(23, 24, 22, 28, 'R2', 18),
(10, 13, 27, 21, 'R1', 40),
(13, 18, 18, 28, 'L1', 35),
(21, 28, 18, 18, 'R1', 22),
(21, 21, 19, 27, 'L1', 23),
(13, 24, 22, 24, 'R2', 29),
(18, 30, 28, 15, 'L2', 25),
(26, 6, 10, 21, 'R1', 27),
(30, 26, 11, 10, 'R2', 41),
(6, 21, 29, 17, 'L2', 40),
(23, 23, 23, 10, 'R2', 31),
(29, 22, 26, 26, 'R1', 35),
(8, 9, 25, 17, 'R1', 35),
(16, 13, 27, 16, 'R1', 19),
(14, 11, 11, 10, 'R2', 34),
(15, 16, 16, 21, 'L2', 19),
(22, 30, 26, 15, 'R2', 20),
(17, 28, 10, 6, 'R2', 28),
(19, 7, 24, 21, 'R2', 36),
(6, 28, 15, 18, 'R2', 28),
(6, 19, 18, 30, 'L2', 26),
(18, 14, 8, 19, 'R1', 32),
(12, 27, 6, 27, 'R1', 21),
(15, 6, 11, 24, 'R1', 31),
(13, 28, 28, 26, 'L1', 28),
(11, 11, 9, 19, 'L2', 33),
(13, 14, 18, 29, 'L2', 27),
(18, 26, 17, 18, 'L1', 42),
(25, 23, 8, 22, 'R1', 22),
(22, 25, 23, 16, 'L1', 28),
(14, 8, 9, 30, 'R2', 19),
(15, 25, 13, 18, 'R2', 25),
(7, 15, 11, 25, 'L1', 18),
(18, 17, 13, 7, 'L2', 35),
(18, 29, 18, 23, 'L2', 28),
(16, 18, 25, 18, 'R2', 28),
(10, 28, 25, 16, 'R1', 25),
(18, 24, 21, 7, 'R2', 41),
(16, 28, 15, 11, 'L1', 23),
(25, 15, 6, 8, 'R1', 20),
(22, 11, 20, 30, 'L2', 36),
(16, 28, 11, 14, 'L2', 19),
(12, 24, 8, 9, 'R2', 21),
(27, 19, 21, 20, 'L2', 28),
(23, 28, 19, 19, 'R2', 23),
(18, 19, 7, 23, 'L1', 19);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assesment`
--
ALTER TABLE `assesment`
  ADD PRIMARY KEY (`assesment_no`),
  ADD KEY `FK_assesment_stud` (`student_no`),
  ADD KEY `FK_assesment_quad` (`dominant_quad`);

--
-- Indexes for table `quadrant`
--
ALTER TABLE `quadrant`
  ADD PRIMARY KEY (`quadrant_code`);

--
-- Indexes for table `qualification`
--
ALTER TABLE `qualification`
  ADD PRIMARY KEY (`q_id`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`question_no`),
  ADD KEY `FK_question_quad` (`quad_code`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`stud_no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assesment`
--
ALTER TABLE `assesment`
  MODIFY `assesment_no` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `qualification`
--
ALTER TABLE `qualification`
  MODIFY `q_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `question_no` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assesment`
--
ALTER TABLE `assesment`
  ADD CONSTRAINT `FK_assesment_quad` FOREIGN KEY (`dominant_quad`) REFERENCES `quadrant` (`quadrant_code`),
  ADD CONSTRAINT `FK_assesment_stud` FOREIGN KEY (`student_no`) REFERENCES `student` (`stud_no`);

--
-- Constraints for table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `FK_question_quad` FOREIGN KEY (`quad_code`) REFERENCES `quadrant` (`quadrant_code`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
