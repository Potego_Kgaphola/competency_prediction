<?php
require 'core/database/Connection.php';
require 'core/database/QueryBuilder.php';

class competencyController{
	protected $queryObject;
	protected $pdo;
	protected $conditions;
	protected $table;
	protected $con;
	protected $config;
	protected $cols;
	public $msg;
	
	public function __construct(){

		$this->config = require 'core/config.php';
		$this->con = new Connection($this->config);
		$this->pdo = $this->con->getConnect();
		$this->queryObject = new QueryBuilder($this->pdo);
		
	}
	
	public function getSection1(){
		$this->cols = array('*' => '');
		$this->table = array('question');
		$this->conditions =  array('quad_code = "L1"');
		$this->parameters = array();
		return  $this->queryObject-> selectQuery($this->table,$this->cols,$this->conditions,$this->parameters);
	}	
	
	public function getSection2(){
		$this->cols = array('*' => '');
		$this->table = array('question');
		$this->conditions =  array('quad_code = "L2"');
		$this->parameters = array();
		return  $this->queryObject-> selectQuery($this->table,$this->cols,$this->conditions,$this->parameters);
	}	
	
	public function getSection3(){
		$this->cols = array('*' => '');
		$this->table = array('question');
		$this->conditions =  array('quad_code = "R1"');
		$this->parameters = array();
		return  $this->queryObject-> selectQuery($this->table,$this->cols,$this->conditions,$this->parameters);
	}	
	
	public function getSection4(){
		$this->cols = array('*' => '');
		$this->table = array('question');
		$this->conditions =  array('quad_code = "R2"');
		$this->parameters = array();
		return  $this->queryObject-> selectQuery($this->table,$this->cols,$this->conditions,$this->parameters);
	}
	
	public function getQ_scores(){
		$this->table = array('quad_score');
		$this->cols = array('*' => '');
		$this->conditions =  array('q_score_value BETWEEN 40 AND 80');
		$this->parameters = array();
		return  $this->queryObject-> selectQuery($this->table,$this->cols,$this->conditions,$this->parameters);
	}
	
	public function getStudent($id){
		$this->table = array('student');
		$this->cols = array('*' => '');
		$this->conditions =  array('stud_no = :stud_no');
		$this->parameters = array(':stud_no' => $id);	
		return $this->queryObject-> selectQuery($this->table,$this->cols,$this->conditions,$this->parameters);
	}	
	public function getCourse($id){
		$this->table = array('qualification');
		$this->cols = array('*' => '');
		$this->conditions =  array('q_id = :q_id');
		$this->parameters = array(':q_id' => $id);	
		return $this->queryObject-> selectQuery($this->table,$this->cols,$this->conditions,$this->parameters);
	}

	public function trainData(){
			$this->table = array('test_data LIMIT 180');//
			$this->conditions =  array();
			$this->cols = array('*' =>'');
			$this->parameters = array();
		return	$this->queryObject->selectQuery($this->table,$this->cols,$this->conditions,$this->parameters);
	}
	public function testData(){
			$this->table = array('test_data');
			$this->conditions =  array('test_id between 380 and 400');
			$this->cols = array('*' =>'');
			$this->parameters = array();
		return	$this->queryObject->selectQuery($this->table,$this->cols,$this->conditions,$this->parameters);
	}

	public function generateData(){
		$this->table = "test_data";	
		$this->conditions = array();
		
		$quad = array('L1','L2','R1','R2');
		$score[4] = array();
		$course[4] = array();
		for($i = 0; $i < 200;$i++){
			
			for($x = 0; $x < 4; $x++){
				$score[$x] = rand(6,30);
			}
			$course = rand(1,4);
			$index = rand(0,3);
			$aps = rand(18,42);
			$this->cols = array(
					'L1_score' => $score[0],
					'L2_score' => $score[1],
					'R1_score' => $score[2],
					'R2_score' => $score[3],
					'dominant' => $quad[$index],
					'Aps' => $aps, 
					'course_id' => $course); 
			$this->queryObject->insertQuery($this->table, $this->cols, $this->conditions);
		}
				
	}
	
	public function insertData($quadL1,$quadL2,$quadR1,$quadR2,$dominant,$student_no){
		
		$this->table = "assesment";		
		$this->cols = array('quad1_score' => $quadL1, 'quad2_score' => $quadL2, 'quad3_score' => $quadR1,'quad4_score' => $quadR2,'dominant_quad' => $dominant,'student_no' => $student_no); 
		$this->conditions = array();
		return $this->queryObject->insertQuery($this->table, $this->cols, $this->conditions);
	}
}
?>