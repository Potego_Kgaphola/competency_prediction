<?php
require 'core/database/Connection.php';
require 'core/database/QueryBuilder.php';

class UserController{
	protected $pdo;
	protected $config;
	protected $table;
	protected $conditions;
	protected $cols;
	protected $queryObject;
	protected $conObject;
	public $msg;

	public function __construct(){
		$this->config = require 'core/config.php';
		$this->conObject = new Connection($this->config);
		$this->pdo = $this->conObject->getConnect();
		$this->queryObject = new QueryBuilder($this->pdo);
		$this->msg = "";
	}

	public function login($studentno, $password,$course){
		$this->table = array('student');
		$this->cols = array('stud_no' => 'stud_no');
		$this->conditions =  array("stud_no = :studentno", "password = :password");
		$this->parameters = array(':studentno' => $studentno, ':password' => $password);

		$user = $this->queryObject-> selectQuery($this->table,$this->cols,$this->conditions,$this->parameters);

		if($this->queryObject->rowCount()){			
			$_SESSION['login'] = true;
			$_SESSION['course'] = $course;
			$_SESSION['stud_no'] = $user[0]['stud_no'];				
			header('Location: index.php');			
		}else{
			$this->msg = "Invalid username and/or password.";
		}
	}
	public function getQualification(){
		$this->cols = array('*' => '');
		$this->table = array('qualification');
		$this->conditions =  array();
		$this->parameters = array();
		return  $this->queryObject-> selectQuery($this->table,$this->cols,$this->conditions,$this->parameters);
	}


}