<script type="text/javascript">
$(function() {
  var ctx, data, myPolarAreaChart, option_bars;
  var L1 = <?php echo json_encode($quadrant_1); ?>;
  var L2 = <?php echo json_encode($quadrant_2); ?>;
  var R1 = <?php echo json_encode($quadrant_3); ?>;
  var R2 = <?php echo json_encode($quadrant_4); ?>;
  Chart.defaults.global.responsive = true;
  ctx = $('#polar-area-chart').get(0).getContext('2d');
  option_bars = {
    scaleShowLabelBackdrop: true,
    scaleBackdropColor: "rgba(255,255,255,0.75)",
    scaleBeginAtZero: true,
    scaleBackdropPaddingY: 2,
    scaleBackdropPaddingX: 2,
    scaleShowLine: true,
    segmentShowStroke: true,
    segmentStrokeColor: "#fff",
    segmentStrokeWidth: 2,
    animationSteps: 150,
    animationEasing: "easeOutBounce",
    animateRotate: true,
    animateScale: false,
    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
  };
  data = [
    {
      value: R1,
      color: "#e9ed17",
      highlight: "#e9ed17",
      label: "R1"
    },{
      value: R2,
      color: "#FA2A00",
      highlight: "#FA2A00",
      label: "R2"
    },{
      value: L2,
      color: "#34c115",
      highlight: "#34c115",
      label: "L2"
    },{
      value: L1,
      color: "#22A7F0",
      highlight: "#22A7F0",
      label: "L1"
    }
  ];
  myPolarAreaChart = new Chart(ctx).PolarArea(data, option_bars);
});

</script>