<script>
function predictL1(quadrant){
	var output;
	           if(quadrant == 'L1')
                output = [1]; 
            else if(quadrant == 'L2')
                output = [0.65];			
            else if(quadrant == 'R1')
                output = [0.4]; 			
            else if(quadrant == 'R2')
				output = [0.1];
	return output;
}
function predictL2(quadrant){
	var output;
	           if(quadrant == 'L2')
                output = [1]; 
            else if(quadrant == 'L1')
                output = [0.65]; 			
            else if(quadrant == 'R1')
                output = [0.4]; 			
            else if(quadrant == 'R2') 
				output = [0.1];
	return output;
}
function predictR1(quadrant){
	var output;
	           if(quadrant == 'R1')
                output = [1]; 
            else if(quadrant == 'R2')
                output = [0.65]; 			
            else if(quadrant == 'L1')
                output = [0.4]; 			
            else if(quadrant == 'L2')
				output = [0.1];
	return output;
}
function predictR2(quadrant){
	var output;
	           if(quadrant == 'R2')
                output = [1]; 
            else if(quadrant == 'R1')
                output = [0.65]; 			
            else if(quadrant == 'L2')
                output = [0.4]; 			
            else if(quadrant == 'L1')			
				output = [0.1];
	return output;
}

function convertToBinaryArray(quad1,quad2,quad3,quad4,aps,quadrant,course) {
    var binaryAPS = aps/42;
    var binary_q1 = quad1/30;
    var binary_q2 = quad2/30;
    var binary_q3 = quad3/30;
    var binary_q4 = quad4/30;
	var binary_course = [];
	var binary = [];
	
	if(course == 1){
		binary_course = [0,1,1,1];
	}else if(course == 2){
		binary_course = [1,0,1,1];
	}else if(course == 3){
		binary_course = [1,1,0,1];
	}else if(course == 4){
		binary_course = [1,1,1,0];
	}	

        if(quadrant == 'L1'){   
           binary = [parseFloat(binaryAPS.toFixed(2)),parseFloat(binary_q1.toFixed(2)),parseFloat(binary_q2.toFixed(2)),parseFloat(binary_q3.toFixed(2)),parseFloat(binary_q4.toFixed(2)),0,0,0,1];
		   Array.prototype.push.apply(binary,binary_course);
		   return binary;
        }
        else if(quadrant == 'L2'){
           binary =  [parseFloat(binaryAPS.toFixed(2)),parseFloat(binary_q1.toFixed(2)),parseFloat(binary_q2.toFixed(2)),parseFloat(binary_q3.toFixed(2)),parseFloat(binary_q4.toFixed(2)),0,0,1,0];
			Array.prototype.push.apply(binary,binary_course);
			return binary;
		}
        else if(quadrant == 'R1'){
           binary =  [parseFloat(binaryAPS.toFixed(2)),parseFloat(binary_q1.toFixed(2)),parseFloat(binary_q2.toFixed(2)),parseFloat(binary_q3.toFixed(2)),parseFloat(binary_q4.toFixed(2)),0,1,0,0];
			Array.prototype.push.apply(binary,binary_course);
			return binary;
		}
        else if(quadrant == 'R2'){
           binary =  [parseFloat(binaryAPS.toFixed(2)),parseFloat(binary_q1.toFixed(2)),parseFloat(binary_q2.toFixed(2)),parseFloat(binary_q3.toFixed(2)),parseFloat(binary_q4.toFixed(2)),1,0,0,0];
			Array.prototype.push.apply(binary,binary_course);
			return binary;
		}

}
var trainingSet = <?php echo json_encode($train_data); ?>;
var competecyNetwork = new synaptic.Architect.Perceptron(
    
    13, // Input layer with 13 neurons
	6, // Hidden layer with 6 neurons
    1  // Output layer with 1 neurons
);
	//perform supervised learning
	var trainingData = [];
    for(var i = 0;i < Object.keys(trainingSet).length; i++) {
            var quad_1 =  trainingSet[i].L1_score;
            var quad_2 =  trainingSet[i].L2_score;
            var quad_3 =  trainingSet[i].R1_score;
            var quad_4 =  trainingSet[i].R2_score;
            var aps_score = trainingSet[i].Aps;
			var dominant_quad = trainingSet[i].dominant;
			//Input 
            var input = convertToBinaryArray(
						quad_1,
						quad_2,
						quad_3,
						quad_4,
						aps_score,
						dominant_quad,
						trainingSet[i].course_id
						);
			
            if(trainingSet[i].course_id == 1)
                output = predictL1(dominant_quad); 
            else if(trainingSet[i].course_id == 2)
                output = predictL2(dominant_quad); 			
            else if(trainingSet[i].course_id == 3)
                output = predictR1(dominant_quad); 						
			else if(trainingSet[i].course_id == 4)
				output = predictR2(dominant_quad); 
			
            trainingData.push({
                input: input,
                output: output
            });
}
// Create trainer
var myTrainer = new synaptic.Trainer(competecyNetwork); 
// Train with training data
myTrainer.train(trainingData, {
    rate: .3,
    iterations: 10000,
	error: .007,
	shuffle: true,
	//log:1,
	cost: synaptic.Trainer.cost.MSE
}); 

var testdata = <?php echo json_encode($test_data); ?>;
for(var x = 0;x < Object.keys(testdata).length; x++){
	var data = convertToBinaryArray(
	testdata[x].L1_score,
	testdata[x].L2_score,
	testdata[x].R1_score,
	testdata[x].R2_score,
	testdata[x].Aps,
	testdata[x].dominant,
	testdata[x].course_id);
	var prediction = competecyNetwork.activate(data);	
	console.log("Dominant brain quadrant: " + testdata[x].dominant + " Qualification ID: " + testdata[x].course_id + " Competency: " + prediction);
}/**/
init();
function init(){
	var score1 = <?php echo json_encode($section1_tot); ?>;
	var score2 = <?php echo json_encode($section2_tot); ?>;
	var score3 = <?php echo json_encode($section3_tot); ?>;
	var score4 = <?php echo json_encode($section4_tot); ?>;
	var stud_aps = <?php echo json_encode($stud_aps); ?>; 
	var dom_quadrant = <?php echo json_encode($quad); ?>;
	var q_code = <?php echo json_encode($qualification_id); ?>;
	var data1 = convertToBinaryArray(score1,score2,score3,score4,stud_aps,dom_quadrant,q_code);
	var p2 = competecyNetwork.activate(data1); 

	window.onload = function () {
    if (! localStorage.justOnce) {
        localStorage.setItem("justOnce", "true");
		window.location.replace("result.php?score1="+score1+"&score2="+score2+"&score3="+score3+"&score4="+score4);
		 }
	}
	document.cookie = "prediction = " + p2;	
}
</script>