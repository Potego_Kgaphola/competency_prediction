function insert(scr,apsAry,qd)
{ 
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("out").innerHTML=xmlhttp.responseText;
    }
  }
//xmlhttp.open("GET","index.php?q="+str,true);
//xmlhttp.send();
}

getUserInfo(<?php echo json_encode($training_data); ?>);

function getUserInfo(userObj){
    alert(userObj[0].quad);
}


function convertToBinaryArray(quadrant,qscore,aps) {
    var binaryAPS = aps/42;
    var binary_qScore = qscore/80;

        if(quadrant == 'L1'){   
           return [binaryAPS.toFixed(2),binary_qScore.toFixed(2),0,0,0,1]; 
        }
        else if(quadrant == 'L2'){
            return [binaryAPS.toFixed(2),binary_qScore.toFixed(2),0,0,1,0];
        }
        else if(quadrant == 'R1'){
            return [binaryAPS.toFixed(2),binary_qScore.toFixed(2),0,1,0,0];
        }
        else if(quadrant == 'R2'){
            return [binaryAPS.toFixed(2),binary_qScore.toFixed(2),1,0,0,0];
        }

}

//function init(){
var competecyNetwork = new synaptic.Architect.Perceptron(
    
   6, // Input layer with 6 neurons
    8, // First hidden layer with 8 neurons
	5, // Second hidden layer with 5 neurons
    1 // Output layer with 1 neurons
);

    var trainingData = [];
    var scores = [];
    var aps = [];
    var quadrants = [];
    var quad = ['L1','L2','R1','R2'];

    for(var i = 1;i < 100; i++) {
            var qScore =  Math.round((Math.random()*(80-40)+40)/10)*10;
            var aps_score = Math.floor(Math.random()*(42 - 18) + 18);
            var index = Math.floor((Math.random() * 4) + 1);
            var input = convertToBinaryArray(quad[index-1],qScore,aps_score); // Input layer

            console.log("quadrant: " + quad[index-1] + " score: " + qScore + " APS: " + aps_score);
            var q  = quad[index-1];
            scores.push({qScore});
            aps.push({aps_score});
            quadrants.push({q});

            if(quad[index-1] == 'L1')
                output = [1]; 
            else if(quad[index-1] == 'L2')
                output = [0.7];  
            else if(quad[index-1] == 'R1')
                output = [0.4];  
            else if(quad[index-1] == 'R2')
                output = [0];  
				else
				output = [0];  
			
            trainingData.push({
                input: input,
                output: output
            });
}

 insert(scores,aps,quadrants);


var myTrainer = new synaptic.Trainer(competecyNetwork); // Create trainer
myTrainer.train(trainingData, {
    rate: 0.2,
    iterations: 10000,
    shuffle: true
}); // Train with training data
//var q = "<?php echo $q_id ?>";
//var s = <?php echo $_GET['q_scores']; ?>;
//var a = <?php echo $_GET['aps_score']; ?>;

var data = convertToBinaryArray('R1',50,30);

console.log("this" + q );
var recommendations = competecyNetwork.activate(data);

var myHTML = document.getElementById("out");
myHTML.innerHTML = '<div class="alert" style ="background:#42d1f4;color:white;" role="alert">';
myHTML.innerHTML += '<p><b>' + (recommendations[0] * 100).toFixed(2) + '%</b> Competent for<hr> ICT - Computer Science<hr> Software Development</p>';
myHTML.innerHTML += '</div>';
// Log neuron outputs

console.log("Copetency Prediction: " + (recommendations * 100) + "%");
/**/
//}