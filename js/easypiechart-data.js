$(function() {
    $('#easypiechart-teal').easyPieChart({
        scaleColor: false,
        barColor: '#34c115'
    });
});

$(function() {
    $('#easypiechart-orange').easyPieChart({
        scaleColor: false,
        barColor: '#e9ed17'
    });
});

$(function() {
    $('#easypiechart-red').easyPieChart({
        scaleColor: false,
        barColor: '#f9243f'
    });
});

$(function() {
   $('#easypiechart-blue').easyPieChart({
       scaleColor: false,
       barColor: '#30a5ff'
   });
});

$(function() {
   $('#easypiechart-black').easyPieChart({
       scaleColor: false,
       barColor: '#000000'
   });
});
 