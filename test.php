<?php
include 'controllers/competencyController.php';
$title = "Competency Prediction";

$run = new competencyController();
$dimensional = $run->trainData();

$quad = array();
foreach($dimensional as $key => $data){
	if($data['L1_score'] > $data['L2_score'] && $data['L1_score'] > $data['R1_score'] && $data['L1_score'] > $data['R2_score'] ){
		$max = $data['L1_score'];
		$quad[$key] = 'L1';
	}	
	elseif($data['L2_score'] > $data['L1_score'] && $data['L2_score'] > $data['R1_score'] && $data['L2_score'] > $data['R2_score'] ){
		$max = $data['L2_score'];
		$quad[$key] = 'L2';
	}	
	elseif($data['R1_score'] > $data['L1_score'] && $data['R1_score'] > $data['L2_score'] && $data['R1_score'] > $data['R2_score'] ){
		$max = $data['R1_score'];
		$quad[$key] = 'R1';
	}
	elseif($data['R2_score'] > $data['L1_score'] && $data['R2_score'] > $data['L2_score'] && $data['R2_score'] > $data['R1_score'] ){
		$max = $data['R2_score'];
		$quad[$key] = 'R2';
	}
}

print_r($quad);